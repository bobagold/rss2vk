<?php
namespace Rss2Vk;
class KvStorage
{
    public function __construct()
    {
        $this->dir = dirname(dirname(__DIR__)) . '/data';
    }
    public function set($key, $value)
    {
        return (bool)file_put_contents("$this->dir/$key", json_encode($value));
    }
    public function get($key)
    {
        $filename = "$this->dir/$key";
        if (!file_exists($filename)) {
            return null;
        }
        return json_decode(file_get_contents($filename), true);
    }
}
