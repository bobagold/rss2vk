<?php
namespace Rss2Vk\Integration;
class KvStorageTest extends \PHPUnit_Framework_TestCase
{
    private function getKvStorage()
    {
        return new \Rss2Vk\KvStorage;
    }
    public function testKv()
    {
        $last = array('dateTime' => '2013-12-12 21:03', 'item' => array());
        $this->assertTrue($this->getKvStorage()->set("last", $last));
        $this->assertEquals($last, $this->getKvStorage()->get("last"));
    }
}
