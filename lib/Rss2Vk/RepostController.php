<?php
namespace Rss2Vk;
class RepostController
{
    private $rss_url;
    public function __construct(array $config)
    {
        $this->rss_url = $config['rss'];
    }
    public function repost()
    {
        $rss = $this->download($this->rss_url);
        $dom = $this->parseRss($rss);
        $kv = new KvStorage();
        $last = $kv->get("last_repost");
        $ret = array();
        $base = $this->getLink($dom);
        foreach ($this->last($dom, $last ? $last['item'] : array(), 1) as $item) {
            $ret[] = $this->item2Vk($item, $base);
            $last['dateTime'] = date("Y-m-d H:i:s");
            $last['item'] = array(
                'guid' => $item->getElementsByTagName('guid')->item(0)->nodeValue,
            );
            $kv->set("last_repost", $last);
        }
        return $ret;
    }
    public function download($url)
    {
        $ch = \curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $ret = curl_exec($ch);
        curl_close($ch);
        return $ret;
    }
    public function parseRss($rss)
    {
        $doc = new \DOMDocument();
        $doc->loadXML($rss);
        return $doc;
    }
    public function last($rss, $last_item, $limit = 0)
    {
        $ret = array();
        foreach ($rss->getElementsByTagName('item') as $node) {
            if ($this->later($node, $last_item)) {
                $ret[] = $node;
            } else {
                break;
            }
        }
        $ret = array_reverse($ret);
        if ($limit) {
            $ret = array_slice($ret, 0, $limit);
        }
        return $ret;
    }
    public function later($a, $b) {
        if ($a instanceof \DomNode && is_array($b)) {
            foreach ($a->childNodes as $child) {
                if (isset($b[$child->nodeName])) {
                    $last_value = $b[$child->nodeName];
                    $value = $child->nodeValue;
                    if (!$this->later($value, $last_value)) {
                        return false;
                    }
                }
            }
            return true;
        }
        if (is_object($b) && is_string($a)) {
            $class = get_class($b);
            $a = new $class($a);
        }
        return $a > $b;
    }
    public function item2Vk($item, $base = '')
    {
        $desc = $item->getElementsByTagName('description')->item(0)->nodeValue;
        $html = new \DOMDocument();
        $html->loadHTML($desc);
        $images = $html->getElementsByTagName('img');
        if ($images->length) {
            return $this->link($images->item(0)->getAttribute('src'), $base);
        }
        $images = $html->getElementsByTagName('a');
        if ($images->length) {
            return $this->link($images->item(0)->getAttribute('href'), $base);
        }
        return $this->text($desc);
    }
    public function getLink($item)
    {
        return $item->getElementsByTagName('link')->item(0)->nodeValue;
    }
    public function link($link, $base = '')
    {
        $base = parse_url($link, PHP_URL_SCHEME) ? '' : $base;
        return array('link' => $base . $link);
    }
    public function text($text)
    {
        return array('text' => $text);
    }
}
