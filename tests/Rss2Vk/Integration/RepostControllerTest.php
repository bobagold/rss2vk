<?php
namespace Rss2Vk\Integration;
class RepostControllerTest extends \PHPUnit_Framework_TestCase
{
    public function testRepost()
    {
        $config = parse_ini_file(__DIR__ . '/config.ini');
        $c = new \Rss2Vk\RepostController($config);
        $dir = getcwd();
        chdir(__DIR__);
        $c->repost();
        chdir($dir);
        $this->markTestIncomplete('test repost output');
    }
    public function testDownload()
    {
        $c = new \Rss2Vk\RepostController(array('rss' => null));
        $dir = getcwd();
        chdir(__DIR__);
        $this->assertEquals(17490, strlen($c->download('file:svalko.rss.xml')));
        chdir($dir);
    }
    public function testParseRss()
    {
        $c = new \Rss2Vk\RepostController(array('rss' => null));
        $this->assertEquals('rss', $c->parseRss(file_get_contents(__DIR__ . '/svalko.rss.xml'))->firstChild->nodeName);
    }
    public function testLast()
    {
        $c = new \Rss2Vk\RepostController(array('rss' => null));
        $rss = $c->parseRss(file_get_contents(__DIR__ . '/svalko.rss.xml'));
        $this->assertCount(2, $c->last($rss, array('guid' => 'http://svalko.org/393515.html')));
        $this->assertCount(2, $c->last($rss, array('pubDate' => new \DateTime('Mon, 09 Dec 2013 20:09:10 +0400'))));
        $this->assertCount(2, $c->last($rss, array('guid' => 'http://svalko.org/393515.html', 'pubDate' => new \DateTime('Mon, 09 Dec 2013 20:09:10 +0400'))));
    }
    public function testLastLimited()
    {
        $c = new \Rss2Vk\RepostController(array('rss' => null));
        $rss = $c->parseRss(file_get_contents(__DIR__ . '/svalko.rss.xml'));
        $this->assertCount(1, $c->last($rss, array('guid' => 'http://svalko.org/393515.html'), 1));
    }
    public function testItem2Vk()
    {
        $c = new \Rss2Vk\RepostController(array('rss' => null));
        $rss = $c->parseRss(file_get_contents(__DIR__ . '/svalko.rss.xml'));
        $this->assertEquals(
            array('link' => 'http://localhost/data/2013_12_09_20_10_tasuka_nm_ru_sva_1.gif'),
            $c->item2Vk(array_shift($c->last($rss, array('guid' => 'http://svalko.org/393515.html'), 1)), 'http://localhost/')
        );
        $this->assertEquals(
            array('text' => 'Школьный кружок исторической реконструкции настолько хорошо воссоздал события ХV века, что восемьдесят человек умерло от чумы.'),
            $c->item2Vk(array_shift($c->last($rss, array('guid' => 'http://svalko.org/392955.html'), 1)))
        );
        $this->assertEquals(
            array('link' => 'http://toxicdump.org/stuff/FourierToy.swf'),
            $c->item2Vk(array_shift($c->last($rss, array('guid' => 'http://svalko.org/392778.html'), 1)))
        );
    }
    public function testGetLink()
    {
        $c = new \Rss2Vk\RepostController(array('rss' => null));
        $rss = $c->parseRss(file_get_contents(__DIR__ . '/svalko.rss.xml'));
        $this->assertEquals('http://svalko.org/', $c->getLink($rss));
    }
}
